package com.mindvalley.mindvalley_kalana_sarange_android_test.Model;

/**
 * Created by Kalana Sarange on 9/11/2016.
 */
public class Category {
    private int id;
    private String title;
    private int photoCount;
    private String links[];

    public Category(int id, String title, int photoCount, String[] links) {
        this.id = id;
        this.title = title;
        this.photoCount = photoCount;
        this.links = links;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getLinks() {
        return links;
    }

    public void setLinks(String[] links) {
        this.links = links;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }
}
