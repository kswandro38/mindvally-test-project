package com.mindvalley.mindvalley_kalana_sarange_android_test.view;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.PinPost;
import com.mindvalley.mindvalley_kalana_sarange_android_test.R;
import com.mindvalley.mindvalley_kalana_sarange_android_test.util.ImageDownloader;
import com.mindvalley.mindvalley_kalana_sarange_android_test.provider.PostKeeper;
import com.mindvalley.mindvalley_kalana_sarange_android_test.util.ImageDownloadListener;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class PinboardActivity extends AppCompatActivity implements ImageDownloadListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinboard);

        /**
         *  Initialize toolbar
         */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Pinterest Pinboard");

        /**
         *  Download data
         */
        try {
            new ImageDownloader(this).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void imageDownloadStart() {

    }

    @Override
    public void imageDownloadFinish(int imageCount, List<PinPost> pinPostList) {
        PostKeeper.imageThumbUrls = new String[imageCount];
        PostKeeper.imageUrls = new String[imageCount];
        for(int i=0; i<imageCount; i++){
            PostKeeper.imageThumbUrls[i]=pinPostList.get(i).getImageUrls().getSmall();
            PostKeeper.imageUrls[i]=pinPostList.get(i).getImageUrls().getRegular();
        }
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(android.R.id.content, new ImageGridFragment(), "PinBoard");
        ft.commit();
    }
}
