package com.mindvalley.mindvalley_kalana_sarange_android_test.Model;

import java.util.Date;
import java.util.List;

/**
 * Created by Kalana Sarange on 9/11/2016.
 */
public class PinPost {
    private String id;
    private Date createdDate;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean likedByUser;
    private User user;
    private ImageUrls imageUrls;
    private List<Category> categoryList;

    public PinPost(String id, Date createdDate, int width, int height, String color, int likes, boolean likedByUser, User user, ImageUrls imageUrls, List<Category> categoryList) {
        this.id = id;
        this.createdDate = createdDate;
        this.width = width;
        this.height = height;
        this.color = color;
        this.likes = likes;
        this.likedByUser = likedByUser;
        this.user = user;
        this.imageUrls = imageUrls;
        this.categoryList = categoryList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ImageUrls getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(ImageUrls imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
