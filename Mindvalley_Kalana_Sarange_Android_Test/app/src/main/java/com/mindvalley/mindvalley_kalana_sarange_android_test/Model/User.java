package com.mindvalley.mindvalley_kalana_sarange_android_test.Model;

/**
 * Created by Kalana Sarange on 9/11/2016.
 */
public class User {
    private String id;
    private String userName;
    private String name;
    private ProfileImage profileImage;
    private Links links;

    public User(String id, String userName, String name, ProfileImage profileImage, Links links) {
        this.id = id;
        this.userName = userName;
        this.name = name;
        this.profileImage = profileImage;
        this.links = links;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProfileImage getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
