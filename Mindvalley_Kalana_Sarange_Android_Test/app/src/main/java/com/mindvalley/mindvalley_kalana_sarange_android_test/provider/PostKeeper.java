/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindvalley.mindvalley_kalana_sarange_android_test.provider;

/**
 * Created by Kalana Sarange on 9/11/2016.
 */
import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.PinPost;

public class PostKeeper {
    public static String[] imageUrls;
    public static String[] imageThumbUrls;
    public static PinPost[] pinPosts;
}
