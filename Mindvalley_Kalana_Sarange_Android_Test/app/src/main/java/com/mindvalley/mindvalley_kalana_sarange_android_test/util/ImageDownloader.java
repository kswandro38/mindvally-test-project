package com.mindvalley.mindvalley_kalana_sarange_android_test.util;

/**
 * Created by Kalana on 8/19/2016.
 */
import android.os.AsyncTask;

import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.Category;
import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.ImageUrls;
import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.Links;
import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.PinPost;
import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.ProfileImage;
import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImageDownloader {
    private static final String IMAGES_URL = "http://pastebin.com/raw/wgkJgazE";
    private ImageDownloadListener imageDownloadListener;

    public ImageDownloader(ImageDownloadListener imageDownloadListener) {
        this.imageDownloadListener=imageDownloadListener;
    }

    public void execute() throws UnsupportedEncodingException {
        imageDownloadListener.imageDownloadStart();
        new DownloadRawData().execute(IMAGES_URL);
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                JSONArray jsonObject = new JSONArray(res);
                storeData(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void storeData(JSONArray jsonArray){
            try {
                int length = jsonArray.length();
                List<PinPost> pinPosts = new ArrayList<>();
                for (int i=0; i<length; i++){
                    JSONObject jsonPinPost = (JSONObject) jsonArray.get(i);
                    JSONObject jsonUser = (JSONObject) jsonPinPost.get("user");
                    JSONObject jsonProfileImages = (JSONObject) jsonUser.get("profile_image");
                    JSONObject jsonLinks = (JSONObject) jsonUser.get("links");
                    JSONObject jsonImageURLs = (JSONObject) jsonPinPost.get("urls");
//                    JSONObject jsonCategories = (JSONObject) jsonPinPost.get("categories");
//
                    List<Category> categoryList = new ArrayList<>();
//                    for (int j=0; j<jsonCategories.length(); j++){
//                        String links[] = new String[2];
//                        links[0] = jsonCategories.getString("self");
//                        links[1] = jsonCategories.getString("photos");
//
//                        categoryList.add(new Category(
//                                jsonCategories.getInt("id"),
//                                jsonCategories.getString("title"),
//                                jsonCategories.getInt("photo_count"),
//                                links
//                                ));
//                    }

                    PinPost pinPost = new PinPost(
                            jsonPinPost.getString("id"),
                            new Date(),
                            jsonPinPost.getInt("width"),
                            jsonPinPost.getInt("height"),
                            jsonPinPost.getString("color"),
                            jsonPinPost.getInt("likes"),
                            jsonPinPost.getBoolean("liked_by_user"),
                            new User(
                                    jsonUser.getString("id"),
                                    jsonUser.getString("username"),
                                    jsonUser.getString("name"),
                                    new ProfileImage(
                                            jsonProfileImages.getString("small"),
                                            jsonProfileImages.getString("medium"),
                                            jsonProfileImages.getString("large")
                                    ),
                                    new Links(
                                            jsonLinks.getString("self"),
                                            jsonLinks.getString("html"),
                                            jsonLinks.getString("photos"),
                                            jsonLinks.getString("likes")
                                    )
                            ),
                            new ImageUrls(
                                    jsonImageURLs.getString("raw"),
                                    jsonImageURLs.getString("full"),
                                    jsonImageURLs.getString("regular"),
                                    jsonImageURLs.getString("small"),
                                    jsonImageURLs.getString("thumb")
                                    ),
                            categoryList
                    );
                    pinPosts.add(pinPost);
                }
                imageDownloadListener.imageDownloadFinish(length, pinPosts);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}

