package com.mindvalley.mindvalley_kalana_sarange_android_test.Model;

/**
 * Created by Kalana Sarange on 9/11/2016.
 */
public class Links {
    private String self;
    private String hrml;
    private String photos;
    private String likes;

    public Links(String self, String hrml, String photos, String likes) {
        this.self = self;
        this.hrml = hrml;
        this.photos = photos;
        this.likes = likes;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getHrml() {
        return hrml;
    }

    public void setHrml(String hrml) {
        this.hrml = hrml;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
