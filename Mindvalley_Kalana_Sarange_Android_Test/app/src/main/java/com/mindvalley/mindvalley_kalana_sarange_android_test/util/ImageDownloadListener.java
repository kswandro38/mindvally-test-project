package com.mindvalley.mindvalley_kalana_sarange_android_test.util;

import com.mindvalley.mindvalley_kalana_sarange_android_test.Model.PinPost;

import java.util.List;

/**
 * Created by Kalana Sarange on 9/11/2016.
 */
public interface ImageDownloadListener {
    void imageDownloadStart();
    void imageDownloadFinish(int imageCount, List<PinPost> pinPostList);
}
